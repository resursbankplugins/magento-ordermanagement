<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Ordermanagement\Block\Adminhtml\System\Config\Callback;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Resursbank\Core\Helper\Scope;
use Resursbank\Ordermanagement\Helper\Config;

/**
 * Displays the callback test result.
 */
class TestResult extends Field
{
    /**
     * @param Context $context
     * @param Config $config
     * @param Scope $scope
     */
    public function __construct(
        Context $context,
        private readonly Config $config,
        private readonly Scope $scope
    ) {
        parent::__construct($context);
    }

    /**
     * Unset some non-related element parameters.
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(
        AbstractElement $element
    ): string {
        /** @noinspection PhpUndefinedMethodInspection */
        /** @phpstan-ignore-next-line */
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    /**
     * Render result text.
     *
     * @param AbstractElement $element
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _getElementHtml(
        AbstractElement $element
    ): string {
        return $this->config->getCallbackTestResult(
            $this->scope->getId(),
            $this->scope->getType()
        );
    }
}
