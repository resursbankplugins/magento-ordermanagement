<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Ordermanagement\Plugin\Core\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Resursbank\Core\Helper\Ecom as Subject;
use Resursbank\Ecom\Lib\Model\PaymentHistory\DataHandler\DataHandlerInterface;
use Resursbank\Ordermanagement\Helper\PaymentHistoryDataHandler;
use Resursbank\Ordermanagement\Helper\Config;
use Resursbank\Core\Helper\Scope;
use Resursbank\Core\Helper\Version;

/**
 * Modify ECom init values.
 */
class Ecom
{
    /**
     * @param PaymentHistoryDataHandler $paymentHistoryDataHandler
     * @param Scope $scope
     * @param Version $version
     * @param Config $config
     */
    public function __construct(
        private readonly PaymentHistoryDataHandler $paymentHistoryDataHandler,
        private readonly Scope $scope,
        private readonly Version $version,
        private readonly Config $config
    ) {
    }

    /**
     * Overwrite payment history data handler to leverage Magento database.
     *
     * @param Subject $subject
     * @param DataHandlerInterface $result
     * @return DataHandlerInterface
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetPaymentHistoryDataHandler(
        Subject $subject,
        DataHandlerInterface $result
    ): DataHandlerInterface {
        return $this->paymentHistoryDataHandler;
    }

    /**
     * Append our module's name to the user-agent string.
     *
     * @param Subject $subject
     * @param string $result
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetUserAgent(
        Subject $subject,
        string $result
    ): string {
        if ($this->config->isAfterShopEnabled(
            scopeCode: $this->scope->getId(),
            scopeType: $this->scope->getType()
        )) {
            return
                $result . ' Resursbank_Ordermanagement ' .
                $this->version->getComposerVersion(
                    module: 'Resursbank_Ordermanagement'
                ) . ' |';
        }

        return $result;
    }
}
