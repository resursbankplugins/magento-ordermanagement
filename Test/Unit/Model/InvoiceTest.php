<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Ordermanagement\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use Magento\Sales\Model\Order\Invoice as InvoiceModel;
use Resursbank\Ordermanagement\Model\Invoice;

/**
 * Unit tests for Model\Invoice.
 */
class InvoiceTest extends TestCase
{
    /**
     * Verify that the invoice getter and setter work.
     *
     * @return void
     */
    public function testSetAndGetInvoice(): void
    {
        $invoiceMock = $this->createMock(
            originalClassName: InvoiceModel::class
        );

        $invoice = new Invoice();

        $invoice->setInvoice(invoice: $invoiceMock);

        $this->assertSame(
            expected: $invoiceMock,
            actual: $invoice->getInvoice()
        );
    }
}
