<?php

declare(strict_types=1);

namespace Resursbank\Ordermanagement\Test\Unit\Helper;

use PHPUnit\Framework\TestCase;
use Resursbank\Ordermanagement\Exception\ResolveOrderStatusFailedException;
use Resursbank\Ordermanagement\Helper\PaymentHistory;

use Magento\Framework\App\Helper\Context;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Resursbank\Core\Helper\Api;
use Resursbank\Ecommerce\Types\OrderStatus;
use Resursbank\Ordermanagement\Api\PaymentHistoryRepositoryInterface;
use Resursbank\Ordermanagement\Helper\ResursbankStatuses;
use Resursbank\Ordermanagement\Model\PaymentHistoryFactory;
use Resursbank\Core\Helper\Order as OrderHelper;

/**
 * Unit tests for the PaymentHistory helper.
 */
class PaymentHistoryTest extends TestCase
{
    /** @var PaymentHistory */
    private PaymentHistory $paymentHistory;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->paymentHistory = new PaymentHistory(
            context: $this->createMock(originalClassName: Context::class),
            paymentHistoryFactory: $this->createMock(
                originalClassName: PaymentHistoryFactory::class
            ),
            paymentHistoryRepository: $this->createMock(
                originalClassName: PaymentHistoryRepositoryInterface::class
            ),
            orderRepo: $this->createMock(
                originalClassName: OrderRepositoryInterface::class
            ),
            api: $this->createMock(originalClassName: Api::class),
            searchBuilder: $this->createMock(
                originalClassName: SearchCriteriaBuilder::class
            ),
            orderHelper: $this->createMock(
                originalClassName: OrderHelper::class
            )
        );
        parent::setUp();
    }

    /**
     * Verify that paymentStatusToOrderStatus correctly maps data.
     *
     * @return void
     * @throws ResolveOrderStatusFailedException
     */
    public function testPaymentStatusToOrderStatusMapping(): void
    {
        $data = [
            OrderStatus::PENDING => ResursbankStatuses::PAYMENT_REVIEW,
            OrderStatus::PROCESSING => ResursbankStatuses::CONFIRMED,
            OrderStatus::COMPLETED => ResursbankStatuses::FINALIZED,
            OrderStatus::ANNULLED => ResursbankStatuses::CANCELLED,
            OrderStatus::CREDITED => Order::STATE_CLOSED
        ];

        foreach ($data as $inputStatus => $expectedResult) {
            static::assertEquals(
                expected: $expectedResult,
                actual: $this->paymentHistory->paymentStatusToOrderStatus(
                    paymentStatus: $inputStatus
                )
            );
        }
    }

    /**
     * Verify that paymentStatusToOrderState correctly maps data.
     *
     * @return void
     * @throws ResolveOrderStatusFailedException
     */
    public function testPaymentStatusToOrderStateMapping(): void
    {
        $data = [
            OrderStatus::PENDING => Order::STATE_PAYMENT_REVIEW,
            OrderStatus::PROCESSING => Order::STATE_PENDING_PAYMENT,
            OrderStatus::COMPLETED => Order::STATE_PROCESSING,
            OrderStatus::ANNULLED => Order::STATE_CANCELED,
            OrderStatus::CREDITED => Order::STATE_CLOSED
        ];

        foreach ($data as $inputStatus => $expectedResult) {
            static::assertEquals(
                expected: $expectedResult,
                actual: $this->paymentHistory->paymentStatusToOrderState(
                    paymentStatus: $inputStatus
                )
            );
        }
    }

    /**
     * Verify that paymentStatusToOrderStatus throws an error by default.
     *
     * @return void
     * @throws ResolveOrderStatusFailedException
     */
    public function testPaymentStatusToOrderStatusThrows(): void
    {
        static::expectException(
            exception: ResolveOrderStatusFailedException::class
        );

        $this->paymentHistory->paymentStatusToOrderStatus(
            paymentStatus: 987654321
        );
    }

    /**
     * Verify that paymentStatusToOrderState throws an error by default.
     *
     * @return void
     * @throws ResolveOrderStatusFailedException
     */
    public function testPaymentStatusToOrderStateThrows(): void
    {
        static::expectException(
            exception: ResolveOrderStatusFailedException::class
        );

        $this->paymentHistory->paymentStatusToOrderState(
            paymentStatus: 987654321
        );
    }
}
