<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Ordermanagement\Controller\Adminhtml\Callback;

use Exception;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Resursbank\Core\Helper\Ecom;
use Resursbank\Core\Helper\Config as CoreConfig;
use Resursbank\Ecom\Exception\Validation\EmptyValueException;
use Resursbank\Ecom\Exception\Validation\MissingKeyException;
use Resursbank\Ecom\Lib\Api\Environment;
use Resursbank\Ecom\Lib\Api\GrantType;
use Resursbank\Ecom\Lib\Model\Network\Auth\Jwt;
use Resursbank\Ordermanagement\Helper\Config;
use Resursbank\Ordermanagement\Helper\Log;
use Resursbank\Ecom\Lib\Api\Scope as ApiScope;
use Resursbank\Ecom\Module\Callback\Repository;
use Resursbank\Ordermanagement\Helper\Ecom\Callback as EcomCallbackHelper;
use Magento\Framework\App\RequestInterface;

/**
 * Force the "test" callback to be triggered at Resurs Bank, to check whether
 * callbacks are properly accepted by the client.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Testecom implements HttpGetActionInterface
{
    /**
     * @param Config $config
     * @param Log $log
     * @param TypeListInterface $cacheTypeList
     * @param ManagerInterface $message
     * @param ResultFactory $resultFactory
     * @param RedirectInterface $redirect
     * @param RequestInterface $request
     * @param Ecom $ecom
     * @param CoreConfig $coreConfig
     * @param EcomCallbackHelper $ecomCallbackHelper
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        private readonly Config $config,
        private readonly Log $log,
        private readonly TypeListInterface $cacheTypeList,
        private readonly ManagerInterface $message,
        private readonly ResultFactory $resultFactory,
        private readonly RedirectInterface $redirect,
        private readonly RequestInterface $request,
        private readonly Ecom $ecom,
        private readonly CoreConfig $coreConfig,
        private readonly EcomCallbackHelper $ecomCallbackHelper,
    ) {
    }

    /**
     * Process request.
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @noinspection BadExceptionsProcessingInspection */
        try {
            $params = $this->request->getParams();
            if (!isset($params['store'])) {
                throw new MissingKeyException(
                    message: 'Request is missing store parameter'
                );
            }
            $store = $params['store'];

            $this->initEcom(store: $store);

            // Trigger the test callback.
            $callbackUrl = $this->ecomCallbackHelper->getUrl(type: 'test');
            $response = Repository::triggerTest(
                url: $callbackUrl
            );

            $this->config->setCallbackTestTriggeredAt(
                scopeId: (int)$store,
                scopeType: ScopeInterface::SCOPE_STORES
            );
            $this->config->setCallbackTestReceivedAt(
                scopeId: (int)$store,
                scopeType: ScopeInterface::SCOPE_STORES
            );
            $this->config->setCallbackTestResult(
                result: $response->status->value,
                scopeId: (int)$store,
                scopeType: ScopeInterface::SCOPE_STORES
            );

            /**
             * Clear cache (to reflect the new "test callback triggered at" date
             * when the config page reloads).
             */
            $this->cacheTypeList->cleanType('config');

            $this->message->addSuccessMessage(__('rb-test-callback-was-sent'));
        } catch (Exception $e) {
            $this->log->exception($e);

            $this->message->addErrorMessage(
                __('rb-test-callback-could-not-be-triggered')
            );
        }

        /** @var Redirect $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $result->setUrl($this->redirect->getRefererUrl());
    }

    /**
     * Initialize Ecom.
     *
     * @param string $store
     * @return void
     * @throws EmptyValueException
     */
    private function initEcom(string $store): void
    {
        $environment = $this->coreConfig->getApiEnvironment(
            scopeCode: $store
        );
        $apiScope = $environment === Environment::PROD ?
            ApiScope::MERCHANT_API :
            ApiScope::MOCK_MERCHANT_API;

        $this->ecom->connect(
            jwtAuth: new Jwt(
                clientId: $this->coreConfig->getClientId(
                    scopeCode: $store
                ),
                clientSecret: $this->coreConfig->getClientSecret(
                    scopeCode: $store
                ),
                grantType: GrantType::CREDENTIALS,
                scope: $apiScope
            )
        );
    }
}
